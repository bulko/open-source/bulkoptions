<?php
/**
 * Bulkoptions
 * @see  https://gitlab.com/bulko/open-source/bulkoption
 * @author Bulko, Golga <r-ro@bulko.net>
 *
 * DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 * TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 * You just DO WHAT THE FUCK YOU WANT TO.
 */

include_once( _PS_MODULE_DIR_ . 'bulkoptions/classes/BulkoptionsManager.php');

class bulkoptions extends Module
{
	private $hooksList;

	function __construct()
	{
		if ( !defined( '_PS_VERSION_' ) )
		{
			exit;
		}
		$this->name = 'bulkoptions';
		$this->version = '3.0.0';
		$this->author = 'Bulko';
		$this->tab = 'administration';
		$this->need_instance = 0;
		$this->bootstrap = true;
		parent::__construct();
		$this->ps_versions_compliancy = [ 'min' => '1.7', 'max' => _PS_VERSION_ ];
		$this->displayName = $this->l('BulkOptions');
		$this->description = $this->l('Module de gestions des options spécifiques à votre thème Prestashop 1.7.');
		$this->confirmUninstall = $this->l('Etes vous sur de vouloir desinstaller le module ?');
		$this->hooksList = [
			'displayHome',
			'displayFooterBefore',
			$this->name . '1',
			$this->name . '2',
			$this->name . '3',
			$this->name . '4',
			$this->name . '5',
			$this->name . '6',
			$this->name . '7',
			$this->name . '8',
			$this->name . '9',
			$this->name . '10',
		];
		include_once( _PS_MODULE_DIR_ . $this->name . '/classes/BulkoptionsManager.php' );
	}
	/*
	 * This function is a overide, do not typed it
	 */
	public function install()
	{
		if (
			!parent::install()
			|| !$this->installTab()
			|| !$this->installDb()
			|| !$this->registerHook('addWebserviceResources')
		)
		{
			return false;
		}
		foreach ( $this->hooksList as $key => $hook )
		{
			if ( !$this->registerHook( $hook ) )
			{
				return false;
			}
		}
		if ( Shop::isFeatureActive() )
		{
			Shop::setContext( Shop::CONTEXT_ALL );
		}
		return true;
	}
	/*
	 * This function is a overide, do not typed it
	 */
	public function uninstall()
	{
		if (
			!parent::uninstall()
			|| !$this->uninstallTab()
			|| !$this->uninstallDb()
			|| !$this->uninstallDbHook()
		)
		{
			return false;
		}
		return true;
	}

	public function uninstallDbHook(): Bool
	{
		$sql = "";
		foreach ( $this->hooksList as $key => $hook )
		{
			if ( strrpos( $hook, 'bulko' ) === 0 )
			{
				$sql .= "DELETE FROM `" . _DB_PREFIX_ . "hook` WHERE `name` = '" . $hook . "'; ";
			}
		}
		if ( !Db::getInstance()->execute($sql) )
		{
			return false;
		}
		return true;
	}

	public function installTab(): Bool
	{
		$tabs = [];
		$tabs["AdminBulkoptions"] = 1;
		$tabs["AdminBulkoptionsBasic"] = 1;
		$tabs["AdminBulkoptionsAdvanced"] = 1;
		$lang = Language::getLanguages();
		foreach ( $tabs as $key => $tab )
		{
			$tab = new Tab();
			$tab->module = $this->name;
			$tab->class_name = $key;
			switch ( $key )
			{
				case 'AdminBulkoptions':
					$tab->id_parent = (int) Tab::getIdFromClassName( 'CONFIGURE' );
					$tab->position = 6;
					foreach ( $lang as $l )
					{
						$tab->name[ $l['id_lang'] ] = $this->displayName;
					}
					break;
				case 'AdminBulkoptionsBasic':
					$tab->id_parent = (int) Tab::getIdFromClassName( 'AdminBulkoptions' );
					$tab->position = 1;
					foreach ( $lang as $l )
					{
						$tab->name[ $l['id_lang'] ] = $this->l("Basic");
					}
					break;
				case 'AdminBulkoptionsAdvanced':
					$tab->id_parent = (int) Tab::getIdFromClassName( 'AdminBulkoptions' );
					$tab->position = 2;
					foreach ( $lang as $l )
					{
						$tab->name[ $l['id_lang'] ] = $this->l("Advanced");
					}
					break;
			}
			$tab->save();
		}
		return true;
	}

	public function uninstallTab(): Bool
	{
		$tab = new Tab( (int) Tab::getIdFromClassName('AdminBulkoptions') );
		$tab->delete();
		$tab = new Tab( (int) Tab::getIdFromClassName('AdminBulkoptionsBasic') );
		$tab->delete();
		$tab = new Tab( (int) Tab::getIdFromClassName('AdminBulkoptionsAdvanced') );
		$tab->delete();
		return true;
	}

	public function installDb(): Bool
	{
		$sql = '
			CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->name .'` (
				`id_' . $this->name . '` INT(11) NOT NULL AUTO_INCREMENT,
				`position` INT(11) NULL,
				`hook` VARCHAR(60) NULL,
				`group` VARCHAR(60) NULL,
				`col_admin` VARCHAR(60),
				`class_admin` VARCHAR(60),
				`class_front` VARCHAR(60),
				`tpl_front` VARCHAR(60),
				`tpl_admin` VARCHAR(60),
				`key` VARCHAR(60) NOT NULL,
				`value` TEXT(3000) NULL,
				`requierd` INT(1),
				`label` VARCHAR(60) NULL,
				`helper` TEXT(3000) NULL,
				`options` TEXT(3000) NULL,
				PRIMARY KEY (`id_' . $this->name . '`)
			) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;
		';
		if( !Db::getInstance()->execute( $sql ) )
		{
			return false;
		}
		return true;
	}

	public function uninstallDb(): Bool
	{
		$sql = 'DROP TABLE ' . _DB_PREFIX_ . $this->name;
		if( !Db::getInstance()->execute( $sql ) )
		{
			return false;
		}
		return true;
	}
	public function __call( String $function, Array $params ): String
	{
		$hookName = str_replace( 'hook', '', $function );
		if( in_array( $hookName, $this->hooksList ) )
		{
			$bulkoptions = new BulkoptionsManager();
			$this->context->smarty->assign([
				'datas' => $bulkoptions->getList( $hookName ),
				'hookName' => $hookName,
			]);
			return $this->display( __FILE__, '/views/templates/hook/hook.tpl' );
		}
	}

	public function getHooksList(): Array
	{
		return $this->hooksList;
	}

	public function getTemplatesList(): Array
	{
		$tpls = scandir( '../modules/' . $this->name . '/views/templates/hook/tpl/' );
		foreach ( $tpls as $k => $tpl )
		{
			if ( $tpl === '.' || $tpl === '..' )
			{
				unset( $tpls[ $k ] );
			}
			else
			{
				$tpls[ $k ] = str_replace( '.tpl', '', $tpls[ $k ] );
			}
		}
		return $tpls;
	}

	public function getAdminTemplatesList(): Array
	{
		$tpls = scandir( '../modules/' . $this->name . '/views/templates/admin/tpl/' );
		foreach ( $tpls as $k => $tpl )
		{
			if ( $tpl === '.' || $tpl === '..' )
			{
				unset( $tpls[ $k ] );
			}
			else
			{
				$tpls[ $k ] = str_replace( '.tpl', '', $tpls[ $k ] );
			}
		}
		return $tpls;
	}

	public function hookAddWebserviceResources()
	{
		return array(
			'bulkoptions' => array(
				'description' => 'Manage bulkoptions',
				'class' => 'BulkoptionsManager'
			)
		);
	}
}
