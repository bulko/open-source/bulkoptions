<?php

/**
 * BulkoptionsManager
 */
class BulkoptionsManager extends ObjectModel
{
	public $id_bulkoptions;
	public $position;
	public $hook;
	public $group;
	public $col_admin;
	public $class_admin;
	public $class_front;
	public $tpl_front;
	public $tpl_admin;
	public $key;
	public $value;
	public $label;
	public $helper;
	public $options;

	public static $definition = [
		'table' => 'bulkoptions',
		'primary' => 'id_bulkoptions',
		'fields' => [
			'id_bulkoptions'	=> 	[ 'type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false ],
			'position'			=>	[ 'type' => self::TYPE_INT, 'size' => 11 ],
			'hook'				=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'group'				=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'col_admin'			=>	[ 'type' => self::TYPE_INT, 'size' => 11 ],
			'class_admin'		=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'class_front'		=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'tpl_front'			=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'tpl_admin'			=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'key'				=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'value'				=>	[ 'type' => self::TYPE_HTML, 'size' => 3000 ],
			'label'				=>	[ 'type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 60 ],
			'helper'			=>	[ 'type' => self::TYPE_HTML, 'size' => 3000 ],
			'options'			=>	[ 'type' => self::TYPE_HTML, 'size' => 3000 ],
		],
	];

	public	function __construct($id_bulkoptions = null)
	{
		parent::__construct($id_bulkoptions);
	}

	public function add($autodate = true, $null_values = false)
	{
		if (!parent::add($autodate, $null_values))
		{
			return false;
		}

		return true;
	}

	public function update($null_values = false)
	{
		return parent::update($null_values);
	}

	static function getOption( String $key )
	{
		$db = Db::getInstance();
		$sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'bulkoptions` WHERE `key` LIKE \'' . $key .'\'';
		$return = $db->executeS($sql);
		if( count( $return ) > 0 )
		{
			return $return[0];
		}
		return false;
	}

	static function getList( String $hookName = null ): Array
	{
		$db = Db::getInstance();
		$sql = 'SELECT * FROM `' . _DB_PREFIX_ . 'bulkoptions`';
		if ( $hookName )
		{
			$sql .= ' WHERE `hook`= "' . $hookName . '"';
		}
		$sql .= ' ORDER BY `hook`, `group`, `position`';
		return $db->executeS( $sql );
	}

	public function isJson( $string )
	{
		json_decode($string);
		return ( json_last_error() == JSON_ERROR_NONE );
	}

	public function saveOption()
	{
		if( !isset( $this->key ) || !isset( $this->value ) )
		{
			throw new Exception("ERROR : Bulkoptions class variable not set properly", 1);
		}
		$get = $this->getOption( $this->key );
		if( $get )
		{
			$this->id = $get['id_bulkoptions'];
			foreach ( $get as $key => $value )
			{
				if ( $key !== 'value' )
				{
					$this->$key = $value;
				}
			}
			$result = $this->update();
		}
		else
		{
			$result = $this->add();
		}
		return $result;
	}
}
