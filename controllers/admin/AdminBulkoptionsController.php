<?php

include_once(_PS_MODULE_DIR_ . 'bulkoptions/classes/BulkoptionsManager.php');

/**
 * AdminBulkoptionsController
 */
class AdminbulkoptionsController extends ModuleAdminController
{
	protected $messages;
	public function __construct()
	{
		$this->bootstrap = true;
		parent::__construct();
		$this->modulePath = '../../../../modules/' . $this->module->name;
		$this->moduleUploadPath = _PS_MODULE_DIR_ . $this->module->name . '/uploads/';
	}

	/**
	 * initContent
	 * @author Danny <d-ds@bulko.net>
	 * @since bulkoptions 1.0.0
	 * @return void
	 */
	public function initContent()
	{
		parent::initContent();
		$this->context->smarty->assign([
			'messages' => $this->messages,
		]);
	}

	/**
	 * init
	 * @author Danny <d-ds@bulko.net>
	 * @since bulkoptions 1.0.0
	 * @return void
	 */
	public function init()
	{
		parent::init();
	}

	public function dateMd5(){
		return md5(date("YmdGis"));
	}
}
