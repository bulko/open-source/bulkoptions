<?php
include_once ( _PS_MODULE_DIR_ . 'bulkoptions/controllers/admin/AdminbulkoptionsController.php' );

/**
 * AdminbulkoptionsAdvanced
 */
class AdminbulkoptionsAdvancedController extends AdminbulkoptionsController
{
	public function __construct()
	{
		parent::__construct();
		$this->messages = [
			[ 'lvl' => 'info', 'msg' => 'Attention vous êtes dans la configuration avancée du module, à manipuler avec précaution.' ]
		];
	}

	public function init()
	{
		if ( !empty( $_POST ) )
		{
			if ( empty( $_GET[ 'updateOption' ] ) )
			{
				$bulkoptions = new BulkoptionsManager();
				$this->messages[] = [ 'lvl' => 'success', 'msg' => 'Option créée avec succès.' ];
			}
			else
			{
				$bulkoptions = new BulkoptionsManager( $_GET[ 'updateOption' ] );
				$this->messages[] = [ 'lvl' => 'success', 'msg' => 'Option modifiée avec succès.' ];
			}
			foreach ( $_POST as $key => $value )
			{
				$bulkoptions->$key = $value;
			}
			$bulkoptions->save();
		}

		if ( !empty( $_GET[ 'deleteOption' ] ) )
		{
			$bulkoptions = new BulkoptionsManager( $_GET[ 'deleteOption' ] );
			$bulkoptions->delete();
			$this->messages[] = [ 'lvl' => 'success', 'msg' => 'Option supprimée avec succès.' ];
		}
		if ( !empty( $_GET[ 'updateOption' ] ) )
		{
			$this->context->smarty->assign([
				'updateOption' => ( array ) new BulkoptionsManager( $_GET[ 'updateOption' ] ),
			]);
		}
		parent::init();
	}

	public function initContent()
	{
		parent::initContent();
		$bulkoptions = new BulkoptionsManager;
		$this->context->smarty->assign([
			'datas'			=> $bulkoptions->getList(),
			'hooks'			=> $this->module->getHooksList(),
			'tpls_front'	=> $this->module->getTemplatesList(),
			'tpls_admin'	=> $this->module->getAdminTemplatesList(),
			'links'	=> [
				'advanced' => $this->context->link->getAdminLink( 'AdminbulkoptionsAdvanced' ),
			],
		]);
		$this->setTemplate( '../../../../modules/' . $this->module->name . '/views/templates/admin/advenced.tpl' );
	}
}
