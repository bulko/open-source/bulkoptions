<?php
include_once ( _PS_MODULE_DIR_ . 'bulkoptions/controllers/admin/AdminbulkoptionsController.php' );

/**
 * AdminbulkoptionsBasic
 */
class AdminbulkoptionsBasicController extends AdminbulkoptionsController
{
	public function __construct()
	{
		parent::__construct();
	}

	public function init()
	{
		if ( !empty( $_FILES ) )
		{
			foreach ( $_FILES as $key => $file )
			{
				if ( !empty( $file['name'] ) && !is_array( $file['name'] ) )
				{

					$bulkoptions = new BulkoptionsManager();
					$bulkoptions->key = $key;
					$bulkoptions->value = _PS_BASE_URL_ . __PS_BASE_URI__ . 'img/' . $this->module->name . '/' . $key . '.jpg?tkn=' . $this->dateMd5();
					$extension = new SplFileInfo( $file['name'] );
					$dimension = getimagesize( $file['tmp_name'] );
					move_uploaded_file( $file['tmp_name'], _PS_IMG_DIR_ . '/' . $this->module->name . '/' . $key . '.jpg' );
					$bulkoptions->saveOption();
					if ( isset( $_POST[ $key ] ) && is_array( $_POST[ $key ] ) )
					{
						$_POST[ $key ][ 'file' ] = _PS_BASE_URL_ . __PS_BASE_URI__ . 'img/' . $this->module->name . '/' . $key . '.jpg?tkn=' . $this->dateMd5();
					}
				}
			}
		}
		if ( !empty( $_POST ) )
		{
			$this->messages[] = [ 'lvl' => 'success', 'msg' => 'Vos options ont été mises à jours.' ];
			foreach ( $_POST as $key => $value )
			{
				$bulkoptions = new BulkoptionsManager();
				$bulkoptions->key = $key;
				if ( is_array( $value ) )
				{
					$value = json_encode( $value );
				}
				$bulkoptions->value = $value;
				$bulkoptions->saveOption();
			}

		}
		parent::init();
	}

	public function initContent()
	{
		$bulkoptions = new BulkoptionsManager;
		$datas = $bulkoptions->getList();
		if ( empty( $datas ) )
		{
			$this->messages[] = [ 'lvl' => 'warning', 'msg' => 'Le module n\'est pas configuré normalement<br /> Merci de vous référer au wiki.' ];
		}
		parent::initContent();
		$this->context->smarty->assign([
			'datas' => $datas,
		]);
		$this->setTemplate( '../../../../modules/' . $this->module->name . '/views/templates/admin/basic.tpl' );
	}
}
