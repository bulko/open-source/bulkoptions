{include file="./messages.tpl"}
<form  method="POST" enctype="multipart/form-data" class="add defaultForm form-horizontal">
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-plus"></i>
					{if empty( $updateOption )}
						Ajouter une option
					{else}
						Modifier une option
					{/if}
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Position</label>
							<div class="col-lg-9">
								<input type="number" class="form-control" name="position" value="{if !empty( $updateOption )}{$updateOption[ 'position' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Hook *</label>
							<div class="col-lg-9">
								<select name="hook" required="">
									{foreach $hooks as $hook}
										{if !empty( $updateOption ) && $updateOption[ 'hook' ] === $hook}
											<option selected="" value="{$hook}">{$hook}</option>
										{else}
											<option value="{$hook}">{$hook}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>

						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Group *</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="group" required="" value="{if !empty( $updateOption )}{$updateOption[ 'group' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Key</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="key" value="{if !empty( $updateOption )}{$updateOption[ 'key' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Template front *</label>
							<div class="col-lg-9">
								<select name="tpl_front" required="">
									{foreach $tpls_front as $tpl_front}
										{if !empty( $updateOption ) && $updateOption[ 'tpl_front' ] === $tpl_front}
											<option selected="" value="{$tpl_front}">{$tpl_front}</option>
										{else}
											<option value="{$tpl_front}">{$tpl_front|ucfirst}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Template admin *</label>
							<div class="col-lg-9">
								<select name="tpl_admin" required="">
									{foreach $tpls_admin as $tpl_admin}
										{if !empty( $updateOption ) && $updateOption[ 'tpl_admin' ] === $tpl_admin}
											<option selected="" value="{$tpl_admin}">{$tpl_admin}</option>
										{else}
											<option value="{$tpl_admin}">{$tpl_admin|ucfirst}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Class front</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="class_front" value="{if !empty( $updateOption )}{$updateOption[ 'class_front' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Class admin</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="class_admin" value="{if !empty( $updateOption )}{$updateOption[ 'class_admin' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Column admin *</label>
							<div class="col-lg-9">
								<select name="col_admin" required="">
									{for $step=1 to 12}
										<option value="{$step}" {if ( !empty( $updateOption ) && $updateOption[ 'col_admin' ] == $step ) || ( empty( $updateOption ) && $step === 12 ) }selected{/if}>col-lg-{$step}</option>
									{/for}
								</select>
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Label</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="label" value="{if !empty( $updateOption )}{$updateOption[ 'label' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Helper</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="helper" value="{if !empty( $updateOption )}{$updateOption[ 'helper' ]}{/if}">
							</div>
						</div>
						<div class="col-lg-6 col-md-12 form-group">
							<label class="control-label col-lg-3">Option</label>
							<div class="col-lg-9">
								<input type="text" class="form-control" name="options" value="{if !empty( $updateOption )}{$updateOption[ 'options' ]}{/if}">
							</div>
						</div>
						<div class="col-md-12 form-group">
							<label class="control-label col-lg-3">Value</label>
							<div class="col-lg-9">
								<textarea class="form-control" name="value" >{if !empty( $updateOption )}{$updateOption[ 'value' ]}{/if}</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					{if empty( $updateOption )}
						<button class="btn btn-default pull-right" type="submit">
							<i class="process-icon-plus"></i>
							Ajouter
						</button>
					{else}
						<button class="btn btn-default pull-right" type="submit">
							<i class="process-icon-save"></i>
							Enregister
						</button>
						<a class="btn btn-default pull-right" href="{$links.advanced}">
							<i class="process-icon-plus"></i>
							Ajouter une nouvelle option
						</a>
					{/if}
				</div>
			</div>
		</div>
	</div>
</form>

{if !empty( $datas )}
<form  method="POST" enctype="multipart/form-data" class="update defaultForm form-horizontal">
	<div class="row">
		<div class="col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-plus"></i>
					Modifier les options
				</div>
				<div class="panel-body">
					<table class="table">
						<thead>
						<tr>
							<th>Position</th>
							<th>Hook</th>
							<th>Group</th>
							<th>TPL Admin</th>
							<th>TPL Front</th>
							<th>Key</th>
							<th>Label</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>
						{foreach $datas as $data}
							<tr>
								<td>{$data.position}</td>
								<td>{$data.hook}</td>
								<td>{$data.group}</td>
								<td>{$data.tpl_admin}</td>
								<td>{$data.tpl_front}</td>
								<td>{$data.key}</td>
								<td>{$data.label}</td>
								<td>
									<a href="{$links.advanced}&deleteOption={$data.id_bulkoptions}" title="Update {$data.id_bulkoptions}" class="btn tooltip-link product-edit">
										<i class="material-icons">mode_delete</i>
									</a>
									<a href="{$links.advanced}&updateOption={$data.id_bulkoptions}" title="Update {$data.id_bulkoptions}" class="btn tooltip-link product-edit">
										<i class="material-icons">mode_edit</i>
									</a>
								</td>
							</tr>
						{/foreach}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</form>
{/if}
