{include file="./messages.tpl"}
<form  method="POST" enctype="multipart/form-data" class="add defaultForm form-horizontal">
	<div class="row">
		<div class="col-md-12">
			{if !empty( $datas )}
				{foreach $datas as $data}
					{if empty( $curentHook ) || $curentHook !== $data.hook}
						{if !empty( $curentHook )}
								</div>
								<div class="clearfix"></div>
								<div class="panel-footer">
									<button class="btn btn-default pull-right" type="submit">
										<i class="process-icon-save"></i>
										Enregistrer
									</button>
								</div>
							</div>
							{assign var="curentGroup" value=''}
						{/if}
						{assign var="curentHook" value=$data.hook}
						<h2 class="titleHook">{$curentHook}</h2>
					{/if}
					{if empty( $curentGroup ) || $curentGroup !== $data.group}
						{if !empty( $curentGroup ) }
								</div>
								<div class="clearfix"></div>
								<div class="panel-footer">
									<button class="btn btn-default pull-right" type="submit">
										<i class="process-icon-save"></i>
										Enregistrer
									</button>
								</div>
							</div>
						{/if}
						{assign var="curentGroup" value=$data.group}
						<div class="panel {$curentGroup}">
							<div class="panel-heading"><h4>{$curentGroup}</h4></div>
							<div class="panel-body">
					{/if}
					{assign var='tplPath' value="./tpl/"|cat:$data.tpl_admin|cat:'.tpl'}
					{include file=$tplPath}
				{/foreach}
					</div>
					<div class="clearfix"></div>
					<div class="panel-footer">
						<button class="btn btn-default pull-right" type="submit">
							<i class="process-icon-save"></i>
							Enregistrer
						</button>
					</div>
				</div>
			{/if}
		</div>
	</div>
</form>

