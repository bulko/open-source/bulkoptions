<div class="row">
	<div class="col-lg-12">
		{if !empty($success)}
			<div class="alert alert-success" role="alert">
				<p>Les données ont bien été envoyées. Afin d'actualiser les informations sur votre site n'oubliez pas de <a href="{$urlToAdminPerformance}&empty_smarty_cache=1&empty_sf2_cache=1" class="alert-link">vider le cache</a> de Prestashop.</p>
			</div>
		{/if}
		{if !empty($errors)}
			<div class="alert alert-danger" role="alert">
				<p>Les données n'ont pas pu être transmises, les erreurs suivantes ont été détectées :</p>
				{foreach from=$errors item=item key=key name=name}
					{if $key === 1}
						<p> - L'image 1 n'a pas une dimension de 300x200 pixel.</p>
					{/if}
					{if $key === 2}
						<p> - L'image 2 n'a pas une dimension de 300x200 pixel.</p>
					{/if}
					{if $key === 3}
						<p> - L'image 3 n'a pas une dimension de 300x200 pixel.</p>
					{/if}
				{/foreach}
			</div>
		{/if}
	</div>
</div>

<form class="defaultForm form-horizontal" action="" method="POST" enctype="multipart/form-data">

	<div class="row">
		<div class="col-lg-6 col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-picture"></i>
					Gestions des images sous le slider
				</div>

				<div class="form-wrapper">
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_img_1">Image 1</label>
						<div class="col-lg-7">
							<input type="file" class="form-control" name="form_img_1" id="form_img_1" value="">
								<p class="help-block">Le fichier doit avoir l'extension en .PNG ainsi qu'un format de 300x200 pixel.</p>
						</div>
						<div class="col-lg-1">
							<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_img_1">Voir l'image</button>

							<div class="modal fade" id="modal_img_1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											 <h4 class="modal-title" id="myModalLabel">Image actuelle</h4>
										</div>
										<div class="modal-body">
											<img src="{$base_url}modules/bulkoptions/views/img/{if isset($data.form_img_1)}{$data.form_img_1}{/if}" class="img-responsive">
										</div>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_title_1"><span title data-toggle="tooltip" class="label-tooltip" data-original-title="Ce texte sera affiché à la place de l'image si celle-ci ne peut pas être téléchargée.">Titre de l'image</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_title_1" id="form_title_1" value="{if isset($data.form_title_1)}{$data.form_title_1}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_alt_1"><span title data-toggle="tooltip" class="label-tooltip" data-original-title="Texte affiché dans l'encart">Texte de l'encart</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_alt_1" id="form_alt_1" value="{if isset($data.form_alt_1)}{$data.form_alt_1}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_link_1">Lien de l'image</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_link_1" id="form_link_1" value="{if isset($data.form_link_1)}{$data.form_link_1}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="">Lien dans un nouvel onglet</label>
						<div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="form_link_check_1" id="form_link_on_1" value="1" {if isset($data.form_link_check_1) && $data.form_link_check_1 === '1' || empty($data.form_link_check_1)}checked="checked"{/if}>
								<label for="form_link_on_1" class="radioCheck">Oui</label>
								<input type="radio" name="form_link_check_1" id="form_link_off_1" value="0" {if isset($data.form_link_check_1) && $data.form_link_check_1 === '0'}checked="checked"{/if}>
								<label for="form_link_off_1" class="radioCheck">Non</label>
								<a class="slide-button btn"></a>
							</span>
						</div>
					</div><br><br>


					<div class="form-group">
						<label class="control-label col-lg-3" for="form_img_2">Image 2</label>
						<div class="col-lg-7">
							<input type="file" class="form-control" name="form_img_2" id="form_img_2" value="">
							<p class="help-block">Le fichier doit avoir l'extension en .PNG ainsi qu'un format de 300x200 pixel.</p>
						</div>
						<div class="col-lg-1">
							<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_img_2">Voir l'image</button>

							<div class="modal fade" id="modal_img_2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											 <h4 class="modal-title" id="myModalLabel">Image actuelle</h4>
										</div>
										<div class="modal-body">
											<img src="{$base_url}modules/bulkoptions/views/img/{if isset($data.form_img_2)}{$data.form_img_2}{/if}" class="img-responsive">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_title_2"><span title data-toggle="tooltip" class="label-tooltip" data-original-title="Ce texte sera affiché à la place de l'image si celle-ci ne peut pas être téléchargée.">Titre de l'image</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_title_2" id="form_title_2" value="{if isset($data.form_title_2)}{$data.form_title_2}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_alt_2"><span title data-toggle="tooltip" class="label-tooltip" data-original-title="Texte affiché dans l'encart">Texte de l'encart</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_alt_2" id="form_alt_2" value="{if isset($data.form_alt_2)}{$data.form_alt_2}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_link_2">Lien de l'image</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_link_2" id="form_link_2" value="{if isset($data.form_link_2)}{$data.form_link_2}{/if}">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-3" for="">Lien dans un nouvel onglet</label>
						<div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="form_link_check_2" id="form_link_on_2" value="1" {if isset($data.form_link_check_2) && $data.form_link_check_2 === '1' || empty($data.form_link_check_2)}checked="checked"}{/if}>
								<label for="form_link_on_2" class="radioCheck">Oui</label>
								<input type="radio" name="form_link_check_2" id="form_link_off_2" value="0" {if isset($data.form_link_check_2) && $data.form_link_check_2 === '0'}checked="checked"{/if}>
								<label for="form_link_off_2" class="radioCheck">Non</label>
								<a class="slide-button btn"></a>
							</span>
						</div>
					</div><br><br>


					<div class="form-group">
						<label class="control-label col-lg-3" for="form_img_3">Image 3</label>
						<div class="col-lg-7">
							<input type="file" class="form-control" name="form_img_3" id="form_img_3" value="">
							<p class="help-block">Le fichier doit avoir l'extension en .PNG ainsi qu'un format de 300x200 pixel.</p>
						</div>
						<div class="col-lg-1">
							<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_img_3">Voir l'image</button>

							<div class="modal fade" id="modal_img_3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
											 <h4 class="modal-title" id="myModalLabel">Image actuelle</h4>
										</div>
										<div class="modal-body">
											<img src="{$base_url}modules/bulkoptions/views/img/{if isset($data.form_img_3)}{$data.form_img_3}{/if}" class="img-responsive">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_title_3"><span title data-toggle="tooltip" class="label-tooltip" data-original-title="Ce texte sera affiché à la place de l'image si celle-ci ne peut pas être téléchargée.">Titre de l'image</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_title_3" id="form_title_3" value="{if isset($data.form_title_3)}{$data.form_title_3}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_alt_3"><span title data-toggle="tooltip" class="label-tooltip" data-original-title="Texte affiché dans l'encart">Texte de l'encart</span></label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_alt_3" id="form_alt_3" value="{if isset($data.form_alt_3)}{$data.form_alt_3}{/if}">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_link_3">Lien de l'image</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_link_3" id="form_link_3" value="{if isset($data.form_link_3)}{$data.form_link_3}{/if}">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-lg-3" for="">Lien dans un nouvel onglet</label>
						<div class="col-lg-9">
							<span class="switch prestashop-switch fixed-width-lg">
								<input type="radio" name="form_link_check_3" id="form_link_on_3" value="1" {if isset($data.form_link_check_3) && $data.form_link_check_3 === '1' || empty($data.form_link_check_3)}checked="checked"}{/if}>
								<label for="form_link_on_3" class="radioCheck">Oui</label>
								<input type="radio" name="form_link_check_3" id="form_link_off_3" value="0" {if isset($data.form_link_check_3) && $data.form_link_check_3 === '0'}checked="checked"{/if}>
								<label for="form_link_off_3" class="radioCheck">Non</label>
								<a class="slide-button btn"></a>
							</span>
						</div>
					</div>

				</div>

				<div class="panel-footer">
					<button class="btn btn-default pull-right" type="submit">
						<i class="process-icon-save"></i>
						Enregistrer
					</button>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-info-sign"></i>
					Emplacement des images
				</div>
				<img src="{$base_url}modules/bulkoptions/views/img/admin/emplacementImgHaut.jpg" class="img-responsive" alt="">
			</div>
		</div>
	</div>


	{* PANEL 2 *}
	<div class="row">
		<div class="col-lg-6 col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-picture"></i>
					Gestion de la vidéo sur la page d'accueil
				</div>

				<div class="form-wrapper">
					<div class="form-group">
						<label class="control-label col-lg-3" for="form_link_4">Url de la vidéo</label>
						<div class="col-lg-9">
							<input type="text" class="form-control" name="form_link_4" id="form_link_4" value='{if isset($data.form_link_4)}{$data.form_link_4}{/if}'>
						</div>
					</div>
				</div>

				<div class="panel-footer">
					<button class="btn btn-default pull-right" type="submit">
						<i class="process-icon-save"></i>
						Enregistrer
					</button>
				</div><br><br>
			</div>
		</div>
		<div class="col-lg-6 col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-info-sign"></i>
					Emplacement de la vidéo
				</div>
				<img src="{$base_url}modules/bulkoptions/views/img/admin/emplacementImgMiddle.jpg" class="img-responsive" alt="">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	{* PANEL 3 *}
	<div class="row">
		<div class="col-lg-6 col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-picture"></i>
					Gestion du texte de présentaion de la page d'accueil
				</div>
				<div class="form-group">
					<label class="control-label col-lg-3" for="home_txt_title"><span title="" data-toggle="tooltip" class="label-tooltip" >Titre</span></label>
					<div class="col-lg-9">
						<textarea class="form-control" name="home_txt_title" id="home_txt_title" rows="4">{if isset($data.home_txt_title)}{$data.home_txt_title}{/if}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-lg-3" for="home_txt"><span title="" data-toggle="tooltip" class="label-tooltip" >Texte</span></label>
					<div class="col-lg-9">
						<textarea class="form-control" name="home_txt" id="home_txt" rows="10">{if isset($data.home_txt)}{$data.home_txt}{/if}</textarea>
					</div>
				</div>

				<div class="panel-footer">
					<button class="btn btn-default pull-right" type="submit">
						<i class="process-icon-save"></i>
						Enregistrer
					</button>
				</div>
			</div>
		</div>

		<div class="col-lg-6 col-md-12">
			<div class="panel">
				<div class="panel-heading">
					<i class="icon-info-sign"></i>
					Emplacement des images
				</div>
				<img src="{$base_url}modules/bulkoptions/views/img/admin/emplacementImgBas.jpg" class="img-responsive" alt="">
			</div>
		</div>
	</div>

</form>
