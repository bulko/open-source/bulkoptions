<div class="form-group col-lg-{$data.col_admin} col-md-12">
	<div class="row">
		<label class="control-label col-lg-3">
			{if empty($data.label)}
				{$data.label}
			{else}
			<span title data-toggle="tooltip" class="label-tooltip" data-original-title="{$data.helper}">
				{$data.label}
			</span>
			{/if}
		</label>
		<div class="col-lg-6">
			<input type="file" name="{$data.key}" value="">
		</div>
		{if !empty($data.value)}
			<div class="col-lg-3">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_img_{$data.key}">Voir l'image</button>
				<div class="modal fade" id="modal_img_{$data.key}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								 <h4 class="modal-title" id="myModalLabel">Image actuelle</h4>
							</div>
							<div class="modal-body">
								<img src="{$data.value}" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
			</div>
		{/if}
	</div>
</div>
