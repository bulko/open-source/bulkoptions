{assign var="dataArr" value=$data.value|json_decode:1}
<div class="form-group col-lg-{$data.col_admin} col-md-12">
	<div class="row">
		{if !empty($data.label)}
			<b class="text-center col-lg-12">
				{$data.label}
			</b>
		{/if}
	</div>
	<hr />
	<div class="row form-group">
		<div class="col-lg-3">
			<select name="{$data.key}[target]">
				<option value="_self">self</option>
				<option {if isset($dataArr.target) && $dataArr.target === "_blank" } selected="selected" {/if} value="_blank">blank</option>
				<option {if isset($dataArr.target) && $dataArr.target === "_parent" } selected="selected" {/if} value="_parent">parent</option>
			</select>
		</div>
		<div class="col-lg-9">
			<input type="text" name="{$data.key}[url]" placeholder="url" value="{$dataArr.url}">
		</div>
	</div>

	<div class="row form-group">
		<div class="col-lg-12">
			<input type="text" name="{$data.key}[txt]" placeholder="txt" value="{$dataArr.txt}">
		</div>
	</div>
	<div class="row form-group">
		<div class="col-lg-8">
			<input type="file" name="{$data.key}" value="">
		</div>
		{if isset($dataArr.file)}
			<input type="hidden" value='{$dataArr.file}' name="{$data.key}[file]" />
			<div class="col-lg-4">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#modal_img_{$data.key}">Voir l'image</button>
				<div class="modal fade" id="modal_img_{$data.key}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								 <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								 <h4 class="modal-title" id="myModalLabel">Image actuelle</h4>
							</div>
							<div class="modal-body">
								<img src="{$dataArr.file}" class="img-responsive">
							</div>
						</div>
					</div>
				</div>
			</div>
		{/if}
	</div>
</div>
