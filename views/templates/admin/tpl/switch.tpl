<div class="form-group col-lg-{$data.col_admin} col-md-12">
	<label class="control-label col-lg-3">
		{if empty($data.label)}
			{$data.label}
		{else}
		<span title data-toggle="tooltip" class="label-tooltip" data-original-title="{$data.helper}">
			{$data.label}
		</span>
		{/if}
	</label>
	<div class="col-lg-9">
		<span class="switch prestashop-switch fixed-width-lg">
			<input type="radio" name="{$data.key}" id="form_link_on_1" value="1" {if isset($data.value) && $data.value === '1' || empty($data.value)}checked="checked"{/if}>
			<label for="form_link_on_1" class="radioCheck">Oui</label>
			<input type="radio" name="{$data.key}" id="form_link_off_1" value="0" {if isset($data.value) && $data.value === '0'}checked="checked"{/if}>
			<label for="form_link_off_1" class="radioCheck">Non</label>
			<a class="slide-button btn"></a>
		</span>
	</div>
</div>
