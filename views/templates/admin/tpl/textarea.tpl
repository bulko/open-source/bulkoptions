<div class="form-group col-lg-{$data.col_admin} col-md-12">
	<label class="control-label col-lg-3">
		{if empty($data.label)}
			{$data.label}
		{else}
		<span title data-toggle="tooltip" class="label-tooltip" data-original-title="{$data.helper}">
			{$data.label}
		</span>
		{/if}
	</label>
	<div class="col-lg-9">
		<textarea name="{$data.key}" cols="30" rows="10">{$data.value}</textarea>
	</div>
</div>
