{assign var="dataArr" value=$data.value|json_decode:1}
<a class="link-img {$data.key} {$data.class_front}" style="background-image: url('{$dataArr.file}');" href="{$dataArr.url}" target="{$dataArr.target}">
	<p class="link-img-txt">
		{$dataArr.txt}
	</p>
</a>
