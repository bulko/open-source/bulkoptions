{if !empty( $data.value) }
	{if !empty( $data.options )}
		<{$data.options} class="{$data.key} title {$data.class_front}">
			{$data.value}
		</{$data.options}>
	{else}
		<p class="{$data.key} {$data.class_front}">
			{$data.value}
		</p>
	{/if}
{/if}
